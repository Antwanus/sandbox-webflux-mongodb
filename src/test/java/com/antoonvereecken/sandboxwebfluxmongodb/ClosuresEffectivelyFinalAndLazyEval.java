package com.antoonvereecken.sandboxwebfluxmongodb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class ClosuresEffectivelyFinalAndLazyEval {

    List<Integer> numbers;

    @BeforeEach
    void createNumbersList() {
        numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
    }

    @Test
    void lambdaExample() {
        numbers.stream()
                .map(number -> number * 2) //lambda is stateless
                .forEach(System.out::println);
    }
    @Test
    void closureExample() {
        Integer multiplier = 2; // lexical scope

        numbers.stream()
                .map(number -> number * multiplier)
                //lambda 'closes over' variable in lexical scope
                // i.e. 'closure'
                .forEach(System.out::println);
    }
    @Test
    void closureUsingFinal() {
        final Integer multiplier = 2;

        Stream<Integer> numberStream = numbers.stream()
                .map(number -> number * multiplier);

        //multiplier = 3;   //ERROR

        numberStream.forEach(System.out::println);
    }
    @Test
    void closureEffectivelyFinal() {
        Integer multiplier = 2; //effectively final

        Stream<Integer> numberStream = numbers.stream()
                .map(number -> number * multiplier);

        //multiplier = 3;  //ERROR

        numberStream.forEach(System.out::println);
    }
    @Test
    void breakingFinal() {
        final Integer[] multiplier = {2};

        // "lazy evaluation"
        Stream<Integer> numberStream = numbers.stream()
                .map(number -> number * multiplier[0]);

        multiplier[0] = 5; // now we can change the value

        numberStream.forEach(System.out::println); // numberStream gets invoked here
    }
}