package com.antoonvereecken.sandboxwebfluxmongodb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

class Java8StreamExamples {
    List<String> dogs;
    List<Integer> numbers;

    @BeforeEach
    void createDogsList() {
        dogs = Arrays.asList("Vizsla", "Lab", "Golden", "GSP", "Poodle", "Yorkie", "Mutt");
    }
    @BeforeEach
    void createNumbersList() {
        numbers = Arrays.asList(1, 2, 3, 4, 5, 6);
    }


    @Test
    void simpleStreamExample() {
        dogs.stream()
                .forEach(System.out::println);
    }

    @Test
    void parallelStreamExample() {
       dogs.parallelStream()
                .forEach(System.out::println);
    }

    @Test
    void mapStreamExample() {
        dogs.stream()
                .map(String::length)
                .forEach(System.out::println);
    }

    @Test
    void filterStreamExample() {
        dogs.stream()
                .filter(s -> s.length() == 6 )
                .forEach(System.out::println);
    }

    @Test
    void filterAndLimitStreamExample() {
        dogs.stream()
                .filter(s -> s.length() == 6 )
                .limit(2)
                .forEach(System.out::println);
    }

    @Test
    void filterAndSortStreamExample() {
        dogs.stream()
                .filter(s -> s.length() == 6 )
                .sorted()
                .forEach(System.out::println);
    }

    @Test
    void filterAndSortStreamWithCollectorExample() {
        String dogString = dogs.stream()
                .filter(s -> s.length() == 6 )
                .sorted()
                .collect(Collectors.joining(", "));

        System.out.println(dogString);
    }

    @Test
    void filterAndSortStreamWithFunctionalComp() {
        String dogString = dogs.stream()
                .filter(s -> s.length() == 6 )
                .limit(3)
                .map(String::toUpperCase)
                .sorted()
                .collect(Collectors.joining(", "));

        System.out.println(dogString);
    }

    @Test
    void testStats() {
       IntSummaryStatistics statistics = numbers.stream()
                .mapToInt(x -> x) //simple operation
                .summaryStatistics();

        System.out.println(statistics.toString());
    }

    @Test
    void testGetMax() {
       OptionalInt max = numbers.stream()
                .mapToInt(x -> x) //simple operation
                .max();

        System.out.println("Max: " + max.getAsInt());
    }

    @Test
    void testSum() {
       int numbersSum = numbers.stream()
                .mapToInt(x -> x) //simple operation
                .sum();

        System.out.println("Sum: " + numbersSum);
    }

    @Test
    void testSumCollector() {
       Integer numbersSum = numbers.stream()
                .collect(Collectors.summingInt(value -> value));

        System.out.println("Sum: " + numbersSum);
    }

    @Test
    void testGroupingBy() {
        Map<Integer, Set<String>> groupedMap = dogs.stream()
                .collect(Collectors.groupingBy(String::length, Collectors.toSet()));

        groupedMap.entrySet().stream()
                .forEach(System.out::println);
    }

    @Test
    void testFlatMap() {
        List<List<Integer>> listOfLists = Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6));

        List<Integer> numbers = listOfLists.stream()
                .flatMap(Collection::stream) //stream numbers of each list, 'flattening' stream
                .collect(Collectors.toList());

        numbers.forEach(System.out::println);
    }

    @Test
    void testReduction() {
        String reducedString = dogs.stream()
                .reduce((a, b) -> a + " - " + b)
                .get(); //terminal operation: returns optional (hence .get())

        System.out.println(reducedString);
    }
}
