package com.antoonvereecken.sandboxwebfluxmongodb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

class ReactiveStreamExamples {

    Flux<String> dogs;

    @BeforeEach
    void createDogsFlux() {
        dogs = Flux.just("Vizsla", "Lab", "Golden", "GSP", "Poodle", "Yorkie", "Mutt");
    }


    @Test
    void simpleStreamExample() {
        dogs.toStream()
                .forEach(System.out::println);
    }

    @Test
    void simpleStreamExample2() {
        dogs.subscribe(System.out::println);
    }

    @Test
    void simpleStreamExample3() {
        //no output (no pressure)
        dogs.doOnEach(dog -> System.out.println(dog.get()));
    }

    @Test
    void simpleStreamExample4() {
        //trigger subscription (=> .subscribe() = pressure)
        dogs.doOnEach(dog -> System.out.println(dog.get())).subscribe();
        // getting a null as last value because I didn't specify whenDone
    }

    @Test
    void simpleStreamExample5WithSubscriber() {
        //trigger subscription
        dogs.subscribe(
                (s -> System.out.println(s)),
                null,
                (() -> System.out.println("Woot! all Done"))
        );
    }

    @Test
    void simpleStreamExample5WithSubscriberConsumers() {
        //subscriber lambda
        Consumer<String> stringConsumerPrintln = System.out::println;
        //error handler
        Consumer<Throwable> throwableConsumerPrintln = e -> System.out.println("Some Error Occurred");
        //runnable upon complete
        Runnable runnableAllDone = () -> System.out.println("Woot! All Done!");

        //trigger subscription
        dogs.subscribe(
                stringConsumerPrintln,
                throwableConsumerPrintln,
                runnableAllDone);
    }

    @Test
    void mapStreamExample() {
        dogs.map(String::length)
                .doOnEach(System.out::println)
                .subscribe();
    }

    @Test
    void filterStreamExample() {
        dogs.filter(s -> s.length() == 6)
                .subscribe(System.out::println);
    }

    @Test
    void filterAndLimitStreamExample() {
        dogs.filter(s -> s.length() == 6)
                .take(2) //limit elements
                .subscribe(System.out::println);
    }

    @Test
    void filterAndSortStreamExample() {
        dogs.filter(s -> s.length() == 6)
                .take(2) //limit elements
                .sort()
                .subscribe(System.out::println);
    }

    @Test
    void filterAndSortStreamWithCollectorExample() {
        dogs.filter(s -> s.length() == 6)
                .take(3) //limit elements
                .sort()
                .collect(Collectors.joining(", ")) //converts to Mono<String>
                .subscribe(System.out::println);
    }

    @Test
    void testFlatMap() {
        Flux<List<List<Integer>>> listFlux = Flux.just(Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6)));

        listFlux.flatMap(lists -> Flux.fromIterable(lists))
                .flatMap(lists -> Flux.fromIterable(lists))
                .subscribe(System.out::println);
    }

    @Test
    void testFlatMap2() {
        Flux<List<List<Integer>>> listFlux = Flux.just(Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6)));

        listFlux.flatMap(lists -> Flux.fromIterable(
                (lists.stream()
                        .flatMap(Collection::stream)
                ).collect(Collectors.toList())))
                .subscribe(System.out::println);
    }

    @Test
    void testReduction() {
        dogs.reduce((a, b) -> a + " - " + b).subscribe(System.out::println);
    }


}
