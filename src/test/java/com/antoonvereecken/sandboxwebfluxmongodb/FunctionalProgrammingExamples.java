package com.antoonvereecken.sandboxwebfluxmongodb;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FunctionalProgrammingExamples {
    /* 4 properties of a function
     *      1. name
     *      2. return type
     *      3. parameter list
     *      4. body
     */
    List<String> dogs;

    @BeforeEach
    void createDogList() {
        dogs = Arrays.asList("Vizsla", "Lab", "Golden", "GSP", "Poodle", "Yorkie", "Mutt");
    }


    @Test
    void functionWith4Things() {

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() { //method
                System.out.println(" In thread t1"); //body
            }
        });

        t1.start();

        System.out.println("In Main Test");
    }
    @Test
    void lambdaExpression() {

        /*
         //lambda expression
        // (parameter list) -> body
                * 1. name - anonymous
                * 2. return type - can be inferred
                * 3. parameter list
                * 4. body
         */
        Thread t1 = new Thread(() -> System.out.println("Silence of the Lambdas"));
        // ^ Constructor is a higher order function,
        // function is a first class citizen

        t1.start();

        System.out.println("In Main Test");
    }

    @Test
    void listIteratorHighCeremony() {
        for (int i = 0; i < dogs.size(); i++) {
            System.out.println(dogs.get(i));
        }
        //very complex, requires a lot of knowledge of code, a lot to go wrong
    }
    @Test
    void listIteratorLessCeremonyExternalIter() {
        for (String doggy : dogs) {
            System.out.println(doggy);
        }
        //simpler, still using external iterator
    }
    @Test
    void listInternalIterConsumer() {
        dogs.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });
    }
    @Test
    void listInternalIterLambdaMethod() {
        dogs.forEach((String s) -> System.out.println(s));
    }

    @Test
    void listInternalIterLambdaMethodTypeInference() {
        dogs.forEach((s) -> System.out.println(s)); //inferred by compiler
    }
    @Test
    void listInternalIterLambdaMethodTypeInferenceJustOne() {
        //drop () if we have one, still need for none or more than one parameter
        dogs.forEach(s -> System.out.println(s)); //inferred by compiler
    }
    @Test
    void listInternalIterLambdaMethodTypeJava8MethodRef() {
        //Java 8 Method Ref
        dogs.forEach(System.out::println);
    }

    @Test
    void countDogsWithSixCharactersImp() {
        /*  Get count of dogs with 6 characters in name - mutation */
        int dogCount = 0;
        for (String dog : dogs) { //external iterator
            if (dog.length() == 6) {
                dogCount++; //note dogCount is mutating (multiple threads accessing this value = can cause problems)
            }
        }
        System.out.println(dogCount);
    }
    @Test
    void countDogsWithSixCharactersDec() {
        /*  Get count of dogs with 6 characters in name - no mutability */
        System.out.println(dogs.stream() //like iterator Java 8 Streams
                .filter(dog -> dog.length() == 6) //filter condition
                .collect(Collectors.toList()) //collect to list
                .size()); //return size
    }
}
