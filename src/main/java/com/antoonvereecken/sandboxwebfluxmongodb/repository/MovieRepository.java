package com.antoonvereecken.sandboxwebfluxmongodb.repository;

import com.antoonvereecken.sandboxwebfluxmongodb.model.Movie;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MovieRepository extends ReactiveMongoRepository<Movie, String> {
}
