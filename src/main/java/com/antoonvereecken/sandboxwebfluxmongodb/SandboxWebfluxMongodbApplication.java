package com.antoonvereecken.sandboxwebfluxmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SandboxWebfluxMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxWebfluxMongodbApplication.class, args);
	}

}
