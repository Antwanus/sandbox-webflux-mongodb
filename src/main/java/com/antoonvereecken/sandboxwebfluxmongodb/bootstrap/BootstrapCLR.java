package com.antoonvereecken.sandboxwebfluxmongodb.bootstrap;

import com.antoonvereecken.sandboxwebfluxmongodb.model.Movie;
import com.antoonvereecken.sandboxwebfluxmongodb.repository.MovieRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import java.util.UUID;

@Component
public class BootstrapCLR implements CommandLineRunner {

    private final MovieRepository movieRepository;


    public BootstrapCLR(MovieRepository movieRepo) {
        this.movieRepository = movieRepo;
    }

    @Override
    public void run(String... args) throws Exception {
        movieRepository.deleteAll().thenMany(
            Flux.just("Silence of the Lambdas", "AEon Flux", "Enter the Mono<Void>", "The Fluxinator",
                    "Back to the Future", "Meet the Fluxes", "Lord or the Fluxes")
                    .map(title -> new Movie(title))
                    .flatMap(movieRepository::save))
                .subscribe(
                        null,
                        null,
                        () -> movieRepository.findAll().subscribe(System.out::println)
                );
    }
}
