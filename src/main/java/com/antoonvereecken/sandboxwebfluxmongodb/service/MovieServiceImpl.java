package com.antoonvereecken.sandboxwebfluxmongodb.service;

import com.antoonvereecken.sandboxwebfluxmongodb.model.Movie;
import com.antoonvereecken.sandboxwebfluxmongodb.model.MovieEvent;
import com.antoonvereecken.sandboxwebfluxmongodb.repository.MovieRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Date;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository repo;

    public MovieServiceImpl(MovieRepository repo) {
        this.repo = repo;
    }

    @Override
    public Flux<MovieEvent> events(String movieId) {
        return Flux.<MovieEvent>generate(movieEventSynchronousSink -> {
            movieEventSynchronousSink.next(new MovieEvent(movieId, new Date()));
        }).delayElements(Duration.ofSeconds(1));
    }

    @Override
    public Mono<Movie> findById(String id) {
        return this.repo.findById(id);
    }

    @Override
    public Flux<Movie> findAll() {
        return this.repo.findAll();
    }
}
