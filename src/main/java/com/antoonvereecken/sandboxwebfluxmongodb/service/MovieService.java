package com.antoonvereecken.sandboxwebfluxmongodb.service;

import com.antoonvereecken.sandboxwebfluxmongodb.model.Movie;
import com.antoonvereecken.sandboxwebfluxmongodb.model.MovieEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {

    Flux<MovieEvent> events(String movieId);

    Mono<Movie> findById(String id);

    Flux<Movie> findAll();
}
